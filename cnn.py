import os
import subprocess
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt


class CNN(nn.Module):
    """
    Convolutional neural network model.

    You may find nn.Conv2d, nn.MaxPool2d and self.add_module useful here.

    :param config: hyper params configuration
    :type config: CNNConfig
    """
    def __init__(self,
                 config):
        super(CNN, self).__init__()
        # YOUR CODE HERE:
        hidden_layers = []
        in_channels = config.channels
        for out_channels, kernel_size, pool_size in zip(config.conv_architecture, config.kernel_sizes, config.pool_kernel):
            hidden_layers.append(nn.Conv2d(in_channels, out_channels, kernel_size))
            hidden_layers.append(nn.ReLU())
            hidden_layers.append(nn.MaxPool2d(pool_size))
            in_channels = out_channels
        self.add_module("hidden_layers", nn.Sequential(*hidden_layers))
        
        f = self.hidden_layers(torch.ones(1, config.channels, config.height, config.width))
        
        linear = []
        in_features = int(np.prod(f.size()[1:]))
        for out_features in config.architecture:
            linear.append(nn.Linear(in_features, out_features))
            if (out_features != config.classes):
                linear.append(nn.ReLU())
                in_features = out_features
        self.add_module("linear", nn.Sequential(*linear))
        
        self.add_module("softmax", nn.Softmax(1))
        # END YOUR CODE

    def forward(self, x):
        """
        Computes forward pass

        :param x: input tensor
        :type x: torch.FloatTensor(shape=(batch_size, number_of_features))
        :return: logits
        :rtype: torch.FloatTensor(shape=[batch_size, number_of_classes])
        """
        # YOUR CODE HERE:
        x = self.hidden_layers(x)
        x = x.view(x.size(0), -1)
        logits = self.linear(x)
        # END YOUR CODE
        return logits

    def predict(self, x):
        """
        Computes model's prediction

        :param x: input tensor
        :type x: torch.FloatTensor(shape=(batch_size, number_of_features))
        :return: model's predictions
        :rtype: torch.LongTensor(shape=[batch_size])
        """
        # YOUR CODE HERE:
        logits = self.forward(x)
        y_hat = self.softmax(logits)
        predictions = torch.argmax(logits, dim=1)
        # END YOUR CODE
        return predictions


def train_model_img_classification(model,
                                   config,
                                   dataholder,
                                   model_path,
                                   verbose=True):
    """
    Train a model for image classification

    :param model: image classification model
    :type model: LogisticRegression or DFN
    :param config: image classification model
    :type config: LogisticRegression or DFN
    :param dataholder: data
    :type dataholder: DataHolder
    :param model_path: path to save model params
    :type model_path: str
    :param verbose: param to control print
    :type verbose: bool
    """
    train_loader = dataholder.train_loader
    valid_loader = dataholder.valid_loader

    best_valid_loss = float("inf")
    # YOUR CODE HERE:
    # i) define the loss criteria and the optimizer. 
    # You may find nn.CrossEntropyLoss and torch.optim.SGD useful here.
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), config.learning_rate, config.momentum)
    # END YOUR CODE

    train_loss = []
    valid_loss = []
    for epoch in range(config.epochs):
        for step, (images, labels) in enumerate(train_loader):
            # YOUR CODE HERE:
            # ii) You should zero the model gradients
            # and define the loss function for the train data.
            optimizer.zero_grad()
            images = images / 255
            logits = model(images)
            loss = criterion(logits, labels)
            # END YOUR CODE
            if step % config.save_step == 0:
                # YOUR CODE HERE:
                # iii) You should define the loss function for the valid data.
                v_images, v_labels = next(iter(valid_loader))
                v_images = v_images / 255
                v_logits = model(v_images)
                v_loss = criterion(v_logits, v_labels)
                # END YOUR CODE
                valid_loss.append(float(v_loss))
                train_loss.append(float(loss))
                if float(v_loss) < best_valid_loss:
                    msg = "\ntrain_loss = {:.3f} | valid_loss = {:.3f}".format(float(loss),float(v_loss))
                    torch.save(model.state_dict(), model_path)
                    best_valid_loss = float(v_loss)
                    if verbose:
                        print(msg, end="")
            # YOUR CODE HERE:
            # iv) You should do the back propagation
            # and do the optimization step.
            loss.backward()
            optimizer.step()         
            # END YOUR CODE
    if verbose:
        x = np.arange(1, len(train_loss) + 1, 1)
        fig, ax = plt.subplots(1, 1, figsize=(12, 5))
        ax.plot(x, train_loss, label='train loss')
        ax.plot(x, valid_loss, label='valid loss')
        ax.legend()
        plt.xlabel('step')
        plt.ylabel('loss')
        plt.title('Train and valid loss')
        plt.grid(True)
        plt.show()
